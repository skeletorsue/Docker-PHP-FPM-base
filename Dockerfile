FROM php:5.5-fpm

RUN apt-get update
RUN apt-get install -y libbz2-dev libcurl4-openssl-dev libxml2-dev libssl-dev
RUN apt-get install -y libpng12-dev libgmp-dev libmcrypt-dev libxslt1-dev
RUN apt-get install -y libmhash-dev

RUN docker-php-ext-install bcmath bz2 calendar ctype curl dba dom exif pdo soap sockets
RUN docker-php-ext-install pdo_mysql ftp gd gettext mcrypt mysql mysqli pcntl shmop
RUN docker-php-ext-install sysvmsg sysvsem sysvshm wddx xsl zip

RUN ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/local/include/
RUN docker-php-ext-configure gmp
RUN docker-php-ext-install gmp

RUN docker-php-ext-configure hash --with-mhash
RUN docker-php-ext-install hash

RUN printf "\n" | pecl install apcu-4.0.11

RUN docker-php-ext-enable apcu

RUN rm -rf /var/lib/apt/lists/*
RUN rm -f /usr/local/etc/php-fpm.d/*.conf

VOLUME /var/run/php5
VOLUME /var/www/html
CMD ["php-fpm"]